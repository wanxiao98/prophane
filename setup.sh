#!/bin/bash

echo "Installing environments"
conda env create --name prophane --file environment.yaml

echo "Please install DBs in the proper way before executing 'run.sh'"
