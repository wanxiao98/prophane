# Prophane: Annotate your metaproteomic search results

The software ca be accessed via a web service ([https://www.prophane.de](https://www.prophane.de)) 
or installed locally. The remainder of this readme explains the usage on a local machine.

## System requirements

- operating system: linux
- RAM: at least 13 GB
- disk space: depends on the installed databases (*e.g.* ~5 GB for PFAMs DB, ~190 GB for ncbi nr)

## Installation

Install time strongly depends on your available bandwidth. The Prophane tool should be installed in
around 15 minutes. Setup of databases can take multiple hours for the large DBs 
(*i.e.* ncbi_nr, uniprot tremble/complete, eggnog)

download Miniconda: 

    wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
    
install Miniconda: 

    bash Miniconda3-latest-Linux-x86_64.sh
    
Prophane: (you might have to install git first: `conda install git`)

    git clone https://gitlab.com/s.fuchs/prophane.git
    
Setup Prophane environment:

    cd prophane
    ./setup.sh

### Database Setup

Databases need manual setup. The required steps are detailed [here](templates/dbs/README.md).

## Run

You need:
- the protein groups from a proteomic search result
- the fasta file containing accessions and sequences that was provided to the proteomic search engine
- optionally: a taxmap to restrict the taxonomic annotation of prophane

An example config file can be found in [templates/config/config.yaml](templates/config/config.yaml). 

All entries of the config file are explained [here](templates/config/README.md).

With config file set up, execute in the prophane directory:

    ./run.sh path/to/your/config.yaml

### Running Example Data

This test run will execute a couple of tests, including one full test of the Prophane pipeline on 
a small example dataset (incl. test databases).
Execution time on a desktop computer is 10-20 minutes. Expected output of the full test can be 
found in `tests/resources/test_full_analysis_and_file_presence/expected-results`.

In the prophane directory, execute:

    ./test.sh    

The first printed line of the test script will show the output directory.

## Folder structure
Location is given relative to the 'output_dir' in job-specific config.yaml.
**annot_type:** annotation type of task, fun: functional annotation, tax: taxonomic annotation
**db_type:** type of database that is used for annotation (e.g. ncbi_nr, eggnog, ...)
**md5sum:** hash sum of task parameters: db_type, database version, annotation algorithm string, parameters read from config params section of respective task
**n:** protein group number
**file:** any file
**taskid:** task number (order of appearance in job config)
**tool:** annotation tool used for the task, e.g. diamond, hmmer, emapper

| location                            | content                                                                                                                                                                                    | filetype |
| -----------------------------       | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------- |
| summary.txt                         | Main result file: summary table of all analyses                                                                                                                                            | TSV      |
| job_info                            | TXT file to store all job information and parameter                                                                                                                                        | TXT      |
| algn/mafft.{n}.txt                  | original ouput of MAFFT                                                                                                                                                                    | TSV      |
| algn/pg.{n}.mafft                   | aligned sequences of protein groups                                                                                                                                                        | FASTA    |
| pgs/protein_groups.yaml             | protein group information: accessions, quantitation, samples                                                                                                                               | YAML     |
| plots/plot_of {annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.html            | interactive krona plots                                                                                                                                                                    | HTML     |
| seqs/all.faa                        | all accessions from the proteomic search result and their respective protein sequences                                                                                                     | FASTA    |
| seqs/missing_taxa.faa               | FASTA file containing sequences of taxonomically undefined proteins (those not covered by the optionally provided taxmap input file. In most cases, this file is the same as seqs/all.faa) | FASTA    |
| seqs/ambiguous_sequences.txt        | Usually not created; accessions with ambiguous sequence information, to help identify issues with input-fasta files.                                                                       | TXT      |
| seqs/missing_sequences.txt          | Usually not created; accessions, which could not be found in provided input FASTA.                                                                                                         | TXT      |
| segs/pg.{n}.faa                     | sequence information of each protein group                                                                                                                                                 | FASTA    |
| tasks/quant.tsv                     | quantification information for each taxonomic or functional lowest common ancestor (LCA)                                                                                                   | TSV      |
| tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.best_hits            | best hits extracted from raw annotation result                                                                                                                                                   | TSV      |
| tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.lca                  | LCA of each protein group                                                                                                                                                                  | TSV      |
| tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.log                  | log of annotation command                                                                                                                                                                  | TXT      |
| tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.map                  | respective accession-annotation linking                                                                                                                                                    | TSV      |
| tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.quant                | LCA of each sample and replicate and their calculated quantification                                                                                                                       | TSV      |
| tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.result               | raw annotation result                                                                                                                                                                      | TXT/TSV  |
| tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.result-cmd.txt       | annotation command, as executed in the shell                                                                                                                                               | TXT      |
| tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.xml                  | input for creation of krona plot                                                                                                                                                           | XML      |
| tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.yaml                 | annotation task parameters (full path to db, annotation algorithm, command line parameters, shortname of task, annotation type)                                                            | YAML     |
| {file}.benchmark.txt                | ressources (io, mem, time) utilized to create {file}                                                                                                                                       | TXT/TSV  |
| tax/taxmap.txt                      | taxonomic information extracted from user-defined taxmaps (see section 2)                                                                                                                  | TSV      |
| tax/missing_taxa_map.txt            | accessions without any taxonomic data/prediction                                                                                                                                           | TXT      |
| tax/ambiguous_taxa.txt              | Usually not created; accessions with ambiguous taxonomies extracted from user-defined taxmaps                                                                                              | TXT      |
