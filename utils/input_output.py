import os
import re
import glob
import yaml
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def load_yaml(filename):
    """
    reads data stored in a yaml file to a dict which is returned
    """
    with open(filename, "r") as handle:
        return yaml.safe_load(handle)


def get_yamls_in_path(path):
    """
    recursively searches 'path' for files matching '*.yaml'
    :param path: string, path that is searched
    :return: list of strings, found yamls
    """
    return [f for f in glob.glob(path + "/**/*.yaml", recursive=True)]


def write_yaml(dictionary, filename, *args, **kwargs):
    with open(filename, 'w') as handle:
        yaml.dump(dictionary, handle, *args, default_flow_style=False, **kwargs)


def is_existing_file_writeable(f):
    return os.access(f, os.W_OK)


def is_path_writable_or_can_be_created(f):
    if os.path.exists(f):
        return is_existing_file_writeable(f)
    else:
        parent_dir = os.path.dirname(f)
        if parent_dir == f:
            return False
        else:
            return is_path_writable_or_can_be_created(parent_dir)


def is_file_top_dir_writeable(f):
    return os.access(os.path.dirname(f), os.W_OK)


def replace_regex_in_file(fname, find, replace, dry_run=False):
    regex = re.compile(find, flags=re.MULTILINE)
    if not is_existing_file_writeable(fname):
        raise PermissionError(f"Could not manipulate file because it is not writeable:\n\t{fname}")
    with open(fname) as handle:
        content = handle.read()
        content = regex.sub(replace, content)
    if dry_run:
        print("Dry running...")
        print(content)
    else:
        with open(fname, "w") as handle:
            handle.write(content)
    logger.info(fname, "adapted")


def iter_file(fname):
    with open(fname, "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) > 0 and line[0] != "#":
                yield line.strip("\r\n")


def get_accs_from_pg_yaml(pg_yaml):
    """get set containing all accessions stored in the defined protein group yaml file"""
    pgs = load_yaml(pg_yaml)
    sep = pgs['style']['protein_sep']
    accs = set()
    for pg in pgs['protein_groups']:
        accs.update(pg['proteins'].split(sep))
    return accs


def get_groups_to_align_from_pg_yaml(pg_yaml):
    data = load_yaml(pg_yaml)
    sep = data['style']['protein_sep']
    pgs = data['protein_groups']
    return [x for x in range(len(pgs)) if sep in pgs[x]['proteins']]


def read_taxmap(taxmap, taxlevel):
    """
    read taxmap into dictionary, only taxlevel information is returned

    :param taxmap: tsv file, mapping accessions to taxa
    :param taxlevel: list of taxlevel names
    :return: dictionary, keys: accessions, values: tax
    """
    taxa = {}
    l = -len(taxlevel)+1
    with open(taxmap, "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) == 0:
                continue
            fields = line.split("\t")
            taxa[fields[0]] = fields[l:]
    return taxa


def get_headline(fname):
    if not os.path.exists(fname):
        raise FileNotFoundError(f"Could not read headline from file: {fname}")
    with open(fname, "r") as handle:
        return handle.readline().lstrip("#").strip()
