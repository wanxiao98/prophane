#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class DatabaseError(Exception):
    """Base class for database associated errors"""
    def __init__(self, database=None):
        self.database = database
        return


class DatabaseAccessorKeyError(DatabaseError):
    """Raised if no database is associated with the given keys"""
    def __init__(self, db_accessor, **keys):
        self.db_accessor = db_accessor
        self.keys = keys

    def __str__(self):
        msg = "\n"
        msg += "No database is associated with the specified keys:\n"
        for key, value in self.keys.items():
            msg += f'\t{key}: {value}\n'
        msg += 'Available databases are:\n'
        msg += str(self.db_accessor)
        return msg


class NotAValidDatabaseConfigError(DatabaseError):
    def __init__(self, yaml):
        self.yaml = yaml

    def __str__(self):
        msg = (
            f"Config file: '{self.yaml}'\n"
            + "\tis not a valid database config."
        )
        return msg


class InputStyleError(Exception):
    """
    Raised upon input style errors
    """
    pass
