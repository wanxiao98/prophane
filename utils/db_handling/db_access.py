import os
import pandas as pd
import sys

from utils.db_handling.databases.helper_classes import Database, DbYamlInteractor
from utils.db_handling.databases import Databases
from utils.db_handling.databases.helper_classes import REQUIRED_DB_SCHEMA_VERSION
from utils.helper_funcs import get_max_version
from utils.exceptions import DatabaseError, DatabaseAccessorKeyError


DATABASE_OVERVIEW_FILE_REL_PATH = "database_overview.tsv"


def is_db_yaml_outdated(db_yaml) -> bool:
    db = DbYamlInteractor(db_yaml)
    return db.is_db_schema_version_outdated()


class DbAccessor(object):
    def __init__(self, db_path):
        """
        Easy access to all databases under given path.
        :param db_path: database base dir, must contain file "database_overview.tsv"
        """
        self.db_base_path = db_path
        self._path_to_database_overview_file = os.path.join(self.db_base_path, DATABASE_OVERVIEW_FILE_REL_PATH)
        self._df_database_infos = self._read_database_info_file()
        self.dbs = Databases(self._df_database_infos["db_yaml"].to_list())
        self._check_for_outdated_db_schema_versions()

    def __str__(self):
        msg = 'Databases available in "{}":\n'.format(self.get_db_base_path())
        df = self._df_database_infos.sort_values(by=['scope', 'db_type']).copy()
        df['db_yaml'] = df['db_yaml'].apply(lambda x: os.path.relpath(os.path.abspath(x), self.get_db_base_path()))
        msg += str(df[['scope', 'db_type', 'db_version', 'db_yaml']].to_string(index=False))
        return msg

    def _read_database_info_file(self):
        try:
            df = pd.read_csv(self._path_to_database_overview_file, sep="\t")
        except FileNotFoundError:
            print(f"Missing database overview file: {self._path_to_database_overview_file}\n"
                  + "To fix, execute 'prophane --db-maintenance'")
            sys.exit(1)
        required_cols = ["db_type", "db_version", "db_yaml", "scope"]
        missing_cols = [k for k in required_cols if k not in df.columns]
        if missing_cols:
            raise TypeError(f"Database overview file lacks required columns: '{missing_cols}'\n"
                            + f"file: {self.db_base_path}")
        df["db_yaml"] = df["db_yaml"].apply(lambda x: os.path.join(self.db_base_path, x))
        return df

    def get_db_base_path(self):
        return self.db_base_path

    def get_db_objects(self):
        return self.dbs.get_db_objects()

    def get_db_obj_for_yaml(self, yaml):
        return self.dbs.get_db_obj_for_yaml(yaml)

    def _check_for_outdated_db_schema_versions(self):
        outdated_db_yamls = []
        for db in self.dbs:
            if db.is_db_schema_version_outdated():
                outdated_db_yamls.append(db.get_yaml())
        if outdated_db_yamls:
            raise DatabaseError(
                f"Database Schema of the following yamls is lower than required (min: {REQUIRED_DB_SCHEMA_VERSION}).\n"
                + "Please execute Database migration.\n"
                + "\n".join(outdated_db_yamls)
            )

    def get_database_object(self, db_type, version='newest'):
        def can_demands_be_satisfied(df, db_type, version):
            if db_type not in df['db_type'].unique():
                return False
            if version != 'newest':
                if version not in df['db_version'].unique():
                    return False
            return True
        df = self._df_database_infos
        if not can_demands_be_satisfied(df, db_type, version):
            raise DatabaseAccessorKeyError(self, db_type=db_type, version=version)
        df = df[df['db_type'] == db_type]
        if version == 'newest':
            version = get_max_version(df['db_version'].values)
        db_obj = self.dbs.get_database_obj_for_type_and_version(db_type, version)
        return db_obj

    def get_database_yaml(self, *args, **kwargs):
        obj = self.get_database_object(*args, **kwargs)
        yaml = obj.get_yaml()
        return yaml

    def print_summary(self):
        print('\n')
        print(self)
        pass

    @staticmethod
    def get_db_schema_version_required_by_prophane():
        return REQUIRED_DB_SCHEMA_VERSION


def get_db_object_for_dbtype_and_version(db_type, db_version, db_base_dir, db_acc=None):
    if not db_acc:
        db_acc = DbAccessor(db_base_dir)
    db_obj = db_acc.get_database_object(db_type, db_version)
    return db_obj
