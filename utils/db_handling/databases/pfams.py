from utils.db_handling.databases.hmm_required_classes import HmmDatabase


class PfamsDatabase(HmmDatabase):
    name = "pfams"
    valid_types = [name]
    mandatory_keys = HmmDatabase.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super(PfamsDatabase, self).__init__(*args, **kwargs)
