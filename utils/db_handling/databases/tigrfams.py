from utils.db_handling.databases.hmm_required_classes import HmmDatabase


class TigrfamsDatabase(HmmDatabase):
    name = "tigrfams"
    valid_types = [name]
    mandatory_keys = HmmDatabase.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super(TigrfamsDatabase, self).__init__(*args, **kwargs)
