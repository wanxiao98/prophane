from utils.db_handling.databases.helper_classes import Database, DatabaseRequiringHelperDbs


class NcbiOrUniprotDatabase(DatabaseRequiringHelperDbs):
    mandatory_keys = DatabaseRequiringHelperDbs.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super(NcbiOrUniprotDatabase, self).__init__(*args, **kwargs)

    def get_fasta_gz_file(self):
        f = self._config["files"]['fasta_gz']
        full_path = self.join_with_db_path(f)
        return full_path

    @property
    def required_db_types(self):
        raise NotImplementedError

    def get_list_of_acc2tax_files(self):
        raise NotImplementedError

    def get_acc2tax_map_file(self):
        return self.get_db_map_file()

    def get_taxdump_file(self):
        return self.get_helper_db_by_type('taxdump').get_taxdump_tar_gz_file()


class NcbiTaxdumpDatabase(Database):
    name = "taxdump"
    valid_types = [name]

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)

    def get_taxdump_tar_gz_file(self):
        return self.join_with_db_path(self._config["files"]["taxdump"])

    def get_tax2annot_map(self):
        # ToDo Is this a good place to encode this path? Should this rather go into the yaml?
        return self.join_with_db_path("taxdump.map.gz")


class NcbiAcc2TaxDatabase(Database):
    name = "accession2taxid"
    valid_types = [name]
    mandatory_keys = Database.mandatory_keys + ['files']

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)

    def get_prot_acc2tax_file(self):
        return self.join_with_db_path(self._config["files"]["prot2taxid"])

    def get_all_acc2tax_files(self):
        paths = [self.join_with_db_path(f) for f in self._config["files"].values()]
        return paths
