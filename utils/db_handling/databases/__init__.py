from .api_class import Databases
from .eggnog import EggnogDatabase
from .ncbi import NcbiNrDatabase
from .ncbi_uniprot_required_classes import NcbiTaxdumpDatabase, NcbiAcc2TaxDatabase
from .pfams import PfamsDatabase
from .tigrfams import TigrfamsDatabase
from .uniprot import UniprotDatabase, UniprotTaxDatabase
