from utils.db_handling.databases.hmm_required_classes import HmmDatabase


class ResfamsDatabase(HmmDatabase):
    name = "resfams"
    valid_types = ["resfams_full", "resfams_core"]
    mandatory_keys = HmmDatabase.mandatory_keys[:]

    def __init__(self, *args, **kwargs):
        super(ResfamsDatabase, self).__init__(*args, **kwargs)
