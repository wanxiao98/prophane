import os
import re
import yaml


rule build_run_emapper_command:
  input:
    query_fasta='seqs/all.faa',
    db_yaml=lambda w: get_db_yaml_for_task(config['tasks'][int(w.taskid)]),
    emapper_data=lambda w: os.path.join(os.path.dirname(get_db_yaml_for_task(config['tasks'][int(w.taskid)])), "data")
  output:
    cmd_file='tasks/{annot_type}_annot_by_emapper_on_{db_type}.task{taskid}.{md5sum}.result-cmd.txt',
    yaml='tasks/{annot_type}_annot_by_emapper_on_{db_type}.task{taskid}.{md5sum}.yaml'
  version:
    "0.1"
  log:
    'tasks/{annot_type}_annot_by_emapper_on_{db_type}.task{taskid}.{md5sum}.log'
  threads:
    32
  run:
    """
    Build the command for annotation of provided query file and write it to text file.
    Write out parameters to task specific yaml file.
    """
    task_no = int(wildcards.taskid)
    task = config['tasks'][task_no]
    query_file = input['query_fasta']
    prog_db_string = input.emapper_data
    annot_result = re.sub(r"-cmd\.txt$", "", output.cmd_file)
    log_file = log[0]

    #no params
    if 'params' not in task:
        task['params'] = {}

    #empty query
    if os.path.getsize(query_file) == 0:
        for o in output:
            open(o, "w").close()
        cmd = ""

    #egnogg mapper
    else:
        single_args = {'m'}
        task['params']['data_dir'] = prog_db_string
        task['params']['output'] = annot_result
        task['params']['cpu'] = threads

        cmd = f"emapper.py -i {query_file} --override"
        cmd += " " + " ".join([f"--{x[0]} {x[1]}" if x[0] not in single_args else f"-{x[0]} {x[1]}" for x in task['params'].items()])

    # write cmd_file
    with open(output.cmd_file, "w") as handle:
        handle.write(cmd)
        handle.write("\n")

    #write yaml
    with open(output.yaml, "w") as handle:
        task = dict(task)
        if params in task:
            task['params'] = dict(task['params'])
        handle.write(yaml.dump(task))


rule run_emapper:
  input:
    cmd_file='tasks/{annot_type}_annot_by_emapper_on_{db_type}.task{taskid}.{md5sum}.result-cmd.txt'
  output:
    'tasks/{annot_type}_annot_by_emapper_on_{db_type}.task{taskid}.{md5sum}.result'
  message:
    "performing task {wildcards.taskid} using emapper..."
  conda:
    "../envs/emapper.yaml"
  log:
    'tasks/{annot_type}_annot_by_emapper_on_{db_type}.task{taskid}.{md5sum}.log'
  benchmark:
    "tasks/{annot_type}_annot_by_emapper_on_{db_type}.task{taskid}.{md5sum}.result.benchmark.txt"
  resources:
    mem_mb=1024*14
  version:
    "0.1"
  threads:
    32
  shell:
    """
    # execute command file
    bash {input.cmd_file}
    # cp eggnog result: output[0] + ".emapper.annotations" -> output[0]
    cp "{output[0]}.emapper.annotations" {output[0]}
    """
