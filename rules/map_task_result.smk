import gzip

from utils.db_handling.databases import NcbiNrDatabase, UniprotDatabase
from utils.helper_funcs_for_snakemake_workflow import get_task_best_hits_file


def input_map_task_result(wildcards):
    task = config['tasks'][int(wildcards.taskid)]
    db_base_dir = get_db_base_dir()
    db_obj = get_db_object_for_task(task)
    db_type = db_obj.get_type()
    db_scope = db_obj.get_scope()

    best_hits_file = get_task_best_hits_file(wildcards.taskid, task, db_base_dir)
    db_map = db_obj.get_db_map_file()

    files = [best_hits_file, db_map]
    # distinguish between nr and uniprot db to append necessary input files for lineage reconstruction
    # TODO this is extremely error prone if db.yaml is not generated automatically
    if (NcbiNrDatabase.is_valid_type(db_type)
        or UniprotDatabase.is_valid_type(db_type)
    ):
        taxmap = db_obj.get_tax2annot_map_file()
        files += [taxmap]
    if db_scope == "tax":
        files.append('tax/taxmap.txt')
    return files


def get_hit_dict(task_best_hits_file, db_type):
    """
    read task_best_hits_file into dictionary
    :param task_best_hits_file: tsv-file, with 2 columns
        column 1: query accession
        column 2: list of target db hit accessions, separated by ';'
    :param db_type: type of the target database
    :return: defaultdict: key: target db hit accession; value: query accession
    """
    hits = defaultdict(set)
    for line in iter_file(task_best_hits_file):
        fields = line.strip().split("\t")
        acc_query = fields[0]
        acc_hits = fields[1]
        for hit in acc_hits.split(";"):
            # disting. between nr and uniprot
            if db_type == "ncbi_nr":
                if hit.count("|") > 0:
                    continue
            elif db_type.startswith("uniprot_"):
                hit = hit.split("|")[-1]
            elif db_type == "pfams":
                hit = hit.split(".")[0]
            hits[hit].add(acc_query)
    return hits


def get_ncbi_nr_taxinfo_dict(db_tax_map):
    """
    read db_tax_map into dict
    :param db_tax_map: ncbi nr taxmap, gzipped
    :return: dictionary:
        key: taxID
        value: string, containing tab-separated entries (zero based index):
            taxid, taxids_lineage, taxlevel[0], taxlevel[1], ...
    """
    taxinfo_dict = {}
    # db_tax_map = taxdump.map.gz
    with gzip.open(db_tax_map, 'rt') as handle:
        # dump headline
        _ =  handle.readline()[1:]
        for line in handle:
            line = line.strip()
            taxid = line.split("\t")[0]
            taxinfo_dict[taxid] = line
    return taxinfo_dict


def get_uniprot_taxinfo_dict(db_tax_map, taxlevels):
    """
    read db_tax_map into dict
    :param db_tax_map: uniprot_tax.txt
    :return: dictionary:
        key: taxID
        value: string, containing tab-separated entries (zero based index):
            taxid, taxids_lineage, taxlevel[0], taxlevel[1], ...
    """
    taxinfo_dict = {}
    taxlevel_count = len(taxlevels)
    # db_tax_map = uniprot_tax.txt
    ranks, parents, names = get_ranks_parents_names_dicts_for_taxids(db_tax_map)
    for taxid in names:
        taxids = ['-'] * taxlevel_count
        lineage = ['-'] * taxlevel_count
        orig_taxid = taxid
        while taxid != "":
            rank = ranks[taxid]
            if rank in taxlevels:
                taxlevel_index = taxlevels.index(rank)
                taxids[taxlevel_index] = taxid
                lineage[taxlevel_index] = names[taxid]
            taxid = parents[taxid]
        taxinfo_dict[orig_taxid] = orig_taxid + "\t" + ";".join(taxids) + "\t" + "\t".join(lineage)
    del names
    del parents
    del ranks
    return taxinfo_dict


def get_ranks_parents_names_dicts_for_taxids(db_tax_map):
    ranks = {}
    parents = {}
    names = {}
    with open(db_tax_map, 'r') as handle:
        handle.readline()
        for line in handle:
            fields = line.strip().split("\t") + [''] * 9
            taxid = fields[0]
            name = fields[2]
            rank = fields[7].lower()
            parent = fields[9]

            names[taxid] = name
            parents[taxid] = parent
            ranks[taxid] = rank
    return ranks, parents, names


def build_ncbi_nr_taskmap_headline_stub(db_tax_map):
    with gzip.open(db_tax_map, 'rt') as handle:
        headline_stub =  handle.readline()[1:]
    return headline_stub


def build_uniprot_taskmap_headline_stub(taxlevels):
    headline_stub = "taxid\ttaxids_lineage\t" + "\t".join(taxlevels) + "\n"
    return headline_stub


def build_db_specific_headline(db_map_containing_annot_level_names, db_type, taxlevels):
    """
    Build headline of the form:
        '#acc\tsource\thit\t(tab-separated-annotation-level-names)\n'
    :param db_map_containing_annot_level_names:
    :param db_type:
    :param taxlevels:
    :return:
    """
    if db_type == "ncbi_nr":
        headline_stub = build_ncbi_nr_taskmap_headline_stub(db_map_containing_annot_level_names)
    elif db_type.startswith("uniprot_"):
        headline_stub = build_uniprot_taskmap_headline_stub(taxlevels)
    else:
        with gzip.open(db_map_containing_annot_level_names, 'rt') as db_map_handle:
            old_headline = db_map_handle.readline().lstrip("#").strip()
            headline_stub = "\t".join(old_headline.split("\t")[1:]) + "\n"
    headline = "#acc\tsource\thit\t" + headline_stub
    return headline


rule map_task_result:
    input:
        input_map_task_result
    output:
        'tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.map'
    message:
        "getting lineage information for best hits"
    benchmark:
        "tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.map.benchmark.txt"
    resources:
        mem_mb=1024*2
    version:
        "0.1"
    run:
        from utils.config import INCONCLUSIVE_ANNOTATIONS
        #get task and db infos
        task_no = int(wildcards.taskid)
        task_dict = config['tasks'][task_no]
        # db_yaml = get_db_yaml_for_task(task_dict)
        db_obj = get_db_object_for_task(task_dict)
        # db_obj = Database(db_yaml)
        db_type = db_obj.get_type()
        db_scope = db_obj.get_scope()
        taxlevels = config['taxlevel']
        task_name = task_dict['shortname']
        unclassified_annotation_string = INCONCLUSIVE_ANNOTATIONS['unclassified']
        db_is_uniprot_or_ncbi_based = (NcbiNrDatabase.is_valid_type(db_type)
                                       or UniprotDatabase.is_valid_type(db_type))

        # files
        task_map = output[0]
        task_best_hits_file = input[0]
        db_acc_to_x_map = input[1]
        if db_is_uniprot_or_ncbi_based:
            db_tax2annot_map = input[2]
            map_containing_annot_level_names = db_tax2annot_map
        else:
            db_tax2annot_map = None
            map_containing_annot_level_names = db_acc_to_x_map
        job_tax_map = None
        taxinfo_dict = None

        # "#acc\tsource\thit\tannotation_info[0]\tannotation_info[1]\t...\n"
        headline = build_db_specific_headline(map_containing_annot_level_names, db_type, taxlevels)
        #get hits; key: accession of target db; values: accessions from query fasta
        hits_dict = get_hit_dict(task_best_hits_file, db_type=db_type)

        if db_scope == "tax":
            job_tax_map = input[-1]
            if db_is_uniprot_or_ncbi_based:
                # reconstruct lineage for nr db
                if db_type == "ncbi_nr":
                    taxinfo_dict = get_ncbi_nr_taxinfo_dict(db_tax2annot_map)
                # reconstruct lineage for uniprot db
                elif db_type.startswith("uniprot_"):
                    taxinfo_dict = get_uniprot_taxinfo_dict(db_tax2annot_map, taxlevels)
                else:
                    raise ValueError('This can not be reached')

        # build a tsv with columns:
        # [#acc, source, hit, annotation_info[0], annotation_info[1], ...]
        with open(task_map, 'w') as outhandle:
            outhandle.write(headline)
            if db_scope == "tax": # write job_tax_map
                for line in open(job_tax_map):
                    outhandle.write(line)
            with gzip.open(db_acc_to_x_map, 'rt') as db_map_handle:
                for line in db_map_handle:
                    fields = line.strip().split("\t")
                    target_db_accession = fields[0]
                    # accession number from map is among best hits
                    if target_db_accession in hits_dict:
                        if db_is_uniprot_or_ncbi_based:
                            taxid = fields[1]
                            lst_annotations_raw = taxinfo_dict[taxid].split("\t")
                        else:
                            lst_annotations_raw = fields[1:]
                        lst_annotations = [unclassified_annotation_string if x == "-" else x for x in lst_annotations_raw]
                        for query in hits_dict[target_db_accession]:
                            outhandle.write(
                                query + "\t" + task_name + " [task " + str(task_no) + "]\t" + "\t".join(lst_annotations) + "\n"
                            )
