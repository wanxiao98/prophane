import re

def best_hit_hmmer3(fname, alg):
    with open(fname, "r") as handle:
        if alg == "hmmsearch":
            col_acc = 0
            col_hit = 3
        elif alg == "hmmscan":
            col_acc = 2
            col_hit = 1
        col_score = 5
        col_bias = 6

        best_hits = {}
        for line in handle:
            if len(line.strip()) == 0 or line[0] == "#":
                continue
            fields = list(filter(None, line.split(" ")))
            acc = fields[col_acc].split(",")[0]
            score = fields[col_score]
            bias = fields[col_bias]
            hit = fields[col_hit]

            if col_acc not in best_hits:
                best_hits[acc] = (hit, score, bias)
            elif score > best_hits[acc][1]:
                best_hits[acc] = (hit, score, bias)
            elif score == best_hits[acc][1] and bias < best_hits[acc][2]:
                best_hits[acc] = (hit, score, bias)
            elif score == best_hits[acc][1] and bias == best_hits[acc][2]:
                trace("error: equal best hits not yet considered in best_hit_hmmer3")

        for acc in best_hits:
            best_hits[acc] = best_hits[acc][0]

        return best_hits

def best_hit_blastp(fname, min_full_ident, db_obj):
    db_specific_acc_re_pattern = db_obj.get_acc_hit_regex_pattern()
    acc_pattern = re.compile("(?:^|;)" + db_specific_acc_re_pattern)
    with open(fname, "r") as handle:
        best_hits = {}
        for line in handle:
            fields = line.strip().split("\t")
            if len(fields) == 0:
                continue
            qacc = fields[0]
            hits = fields[1]
            qlen = int(fields[2])
            bitscore = float(fields[4])
            algnlen = int(fields[5])
            nident = int(fields[6])
            full_ident = algnlen / qlen * nident
            if full_ident >= min_full_ident:
                if qacc not in best_hits:
                    best_hits[qacc] = (hits, bitscore, full_ident)
                elif bitscore > best_hits[qacc][1]:
                    best_hits[qacc] = (hits, bitscore, full_ident)
                elif bitscore == best_hits[qacc][1] and full_ident > best_hits[qacc][2]:
                    best_hits[qacc] = (hits, bitscore, full_ident)
                elif bitscore == best_hits[qacc][1] and full_ident == best_hits[qacc][2]:
                    best_hits[qacc] = (best_hits[qacc][0] + ";" + hits, bitscore, full_ident)

        for acc in best_hits:
            hits = best_hits[acc][0]
            best_hits[acc] = []
            for match in  acc_pattern.finditer(hits):
                best_hits[acc].append(match.group(1))

        return best_hits

def best_hit_emapper(fname):
    with open(fname, "r") as handle:
        best_hits = {}
        for line in handle:
            line = line.strip()
            if len(line) == 0 or line[0] == "#":
                continue
            fields = line.split("\t")
            best_hits[fields[0]] = fields[10].split("|")[0] + "." + fields[1]
    return best_hits

rule get_besthits:
  input:
    'tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.result'
  output:
    'tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.best_hits'
  message:
    "collecting best hits of task {wildcards.taskid} ..."
  version:
    "0.1"
  threads:
    1
  run:
    task = config['tasks'][int(wildcards.taskid)]
    query_file = input[0]
    db_obj = get_db_object_for_task(task)

    #diamond blastp
    if task['prog'] == "diamond blastp":
        out = []
        if "query-cover" in task:
          qc = task["query-cover"]
        elif "query-cover" in task['params']:
          qc = task['params']['query-cover']
        else:
          qc = 0
        for acc, hits in best_hit_blastp(input[0], qc, db_obj).items():
            for hit in hits:
                out.append(acc + '\t' + hit)
        with open(output[0], "w") as handle:
            handle.write("\n".join(out))

    #hmmer
    elif task['prog'] in ["hmmscan", "hmmsearch"]:
        with open(output[0], "w") as handle:
            handle.write("\n".join([x[0] + "\t" + x[1] for x in best_hit_hmmer3(input[0], task['prog']).items()]))

    #egnogg mapper
    elif task['prog'] == "emapper":
        with open(output[0], "w") as handle:
            handle.write("\n".join([x[0] + "\t" + x[1] for x in best_hit_emapper(input[0]).items()]))

    #unknown tools
    else:
        trace("error: task " + wildcards.taskid + ": '" + task['prog'] + "' not allowed.", True)
