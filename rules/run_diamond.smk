import os
import re
import yaml


rule build_run_diamond_command:
  input:
    query_fasta='seqs/missing_taxa.faa',
    db_yaml=lambda w: get_db_yaml_for_task(config['tasks'][int(w.taskid)]),
    dmnd_db=lambda w: os.path.splitext(get_db_yaml_for_task(config['tasks'][int(w.taskid)]))[0] + ".dmnd"
  output:
    cmd_file='tasks/{annot_type}_annot_by_diamond_on_{db_type}.task{taskid}.{md5sum}.result-cmd.txt',
    yaml='tasks/{annot_type}_annot_by_diamond_on_{db_type}.task{taskid}.{md5sum}.yaml'
  version:
    "0.1"
  log:
    'tasks/{annot_type}_annot_by_diamond_on_{db_type}.task{taskid}.{md5sum}.log'
  threads:
    32
  run:
    """
    Build the command for annotation of provided query file and write it to text file.
    Write out parameters to task specific yaml file.
    """
    task_no = int(wildcards.taskid)
    task = config['tasks'][task_no]
    query_file = input['query_fasta']
    prog_db_string = input.dmnd_db
    annot_result = re.sub(r"-cmd\.txt$", "", output.cmd_file)
    log_file = log[0]

    #no params
    if 'params' not in task:
        task['params'] = {}

    #empty query
    if os.path.getsize(query_file) == 0:
        for o in output:
            open(o, "w").close()
        cmd = ""

    else:
        task['params']['outfmt'] = '6 qseqid sallseqid qlen evalue bitscore length nident'
        task['params']['out'] = annot_result
        task['params']['query'] = query_file
        task['params']['db'] = prog_db_string
        task['params']['threads'] = threads
        task['params']['top'] = 0

        cmd = task['prog'] + " " + " ".join([f"--{x[0]} {x[1]}" for x in task['params'].items()])
        err_cmd = f"2> {log_file}"
        cmd += " " + err_cmd

    # write cmd_file
    with open(output.cmd_file, "w") as handle:
        handle.write(cmd)
        handle.write("\n")

    #write yaml
    with open(output.yaml, "w") as handle:
        task = dict(task)
        if params in task:
            task['params'] = dict(task['params'])
        handle.write(yaml.dump(task))


rule run_diamond:
  input:
    cmd_file='tasks/{annot_type}_annot_by_diamond_on_{db_type}.task{taskid}.{md5sum}.result-cmd.txt'
  output:
    'tasks/{annot_type}_annot_by_diamond_on_{db_type}.task{taskid}.{md5sum}.result'
  message:
    "performing task {wildcards.taskid} using diamond..."
  conda:
    "../envs/diamond.yaml"
  log:
    'tasks/{annot_type}_annot_by_diamond_on_{db_type}.task{taskid}.{md5sum}.log'
  benchmark:
    "tasks/{annot_type}_annot_by_diamond_on_{db_type}.task{taskid}.{md5sum}.result.benchmark.txt"
  resources:
    mem_mb=1024*14
  version:
    "0.1"
  threads:
    32
  shell:
    """
    bash {input.cmd_file}
    """
