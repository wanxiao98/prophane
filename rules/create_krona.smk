import os


rule create_krona_xml:
  input:
    'tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.quant'
  output:
    'tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.xml'
  message:
    "creating plot xml ..."
  version:
    "0.2"
  conda:
    "../envs/krona.yaml"
  params:
    script_path=os.path.join(workflow.basedir, "scripts", "format4krona.py")
  shell:
    "{params.script_path} {input[0]}"


rule create_krona_html:
  input:
    'tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.xml'
  output:
    "plots/plot_of_{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.html"
  message:
    "creating plot html ..."
  version:
    "0.2"
  conda:
    "../envs/krona.yaml"
  shell:
    "ktImportXML -o {output[0]} {input[0]}"
