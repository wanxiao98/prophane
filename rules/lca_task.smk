from collections import defaultdict

def get_lca(data, heterog_label):
    data = list(zip(*data))
    heterog = False
    for t in range(len(data)):
        if heterog:
            data[t] = heterog_label
        elif len(set(data[t])) == 1:
            data[t] = data[t][0]
        else:
            data[t] = heterog_label
            heterog = True
    return "\t".join(data)


rule lca_task:
  input:
    pg_yaml='pgs/protein_groups.yaml',
    result_map='tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.map'
  output:
    'tasks/{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}.lca'
  message:
    "inferring lowest common ancestor ..."
  run:
    from utils.config import INCONCLUSIVE_ANNOTATIONS
    # process task map
    task = config['tasks'][int(wildcards.taskid)]
    db_obj = get_db_object_for_task(task)
    n_lca_levels = db_obj.get_no_of_lca_levels()
    heterogeneous_annotation_string = INCONCLUSIVE_ANNOTATIONS['heterogenous']

    hit_classes_dict = defaultdict(list)
    # headline of lca level names
    lca_level_names = "\t".join(get_headline(input.result_map).split("\t")[-n_lca_levels:])
    with open(input.result_map, "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) == 0:
                continue
            if line[0] == "#":
                continue
            fields = line.split("\t")
            query_accession = fields[0]
            lca_levels = fields[-n_lca_levels:]
            hit_classes_dict[query_accession].append(lca_levels)

    #analyse pgs
    pgs = load_yaml(input.pg_yaml)
    sep = pgs['style']['protein_sep']
    with open(output[0], "w") as handle:
        handle.write("#pg\t" + lca_level_names + "\n")
        for protein_group_number, pg_dict in enumerate(pgs['protein_groups']):
            group_classes = []
            for acc in pg_dict['proteins'].split(sep):
                if acc not in hit_classes_dict:
                    group_classes.append([INCONCLUSIVE_ANNOTATIONS['unclassified']] * n_lca_levels)
                else:
                    group_classes.extend(hit_classes_dict[acc])
            handle.write(
                str(protein_group_number) + "\t" + get_lca(group_classes, heterogeneous_annotation_string) + "\n"
            )
