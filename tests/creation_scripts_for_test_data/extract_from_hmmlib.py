#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(prog='...', description='extracts hmm from lib file')
parser.add_argument('-l', metavar="LIB", help="hmmlib file containing hmms", type=argparse.FileType('r'), required=True)
parser.add_argument('-k', metavar="STR", help="keywords (if found hmm is considered)", type=str, nargs='+', required=True)
args = parser.parse_args()

def intersect(keywords, haystack):
    l = "".join(haystack)
    return len([True for x in keywords if x in l]) > 0

entry = []
keywords = set(args.k)
for line in args.l:
    entry.append(line)
    if line.strip() == "//":
        entry = "".join(entry)
        if intersect(keywords, entry):
            print(entry)
        entry = []
