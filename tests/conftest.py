from tests.test_integration import copy, PROPHANE_DIR, get_files_in_dir
import os
import pytest


def setup_test_dbs(source_db_dir, target_dir):
    from tests import setup_test
    source_base_dir_name = os.path.basename(os.path.normpath(source_db_dir))
    copy(source_db_dir, str(target_dir))
    new_db_dir = str(target_dir.join(source_base_dir_name))
    setup_test.set_db_config_paths(
        db_base_dir=new_db_dir
    )
    return new_db_dir


@pytest.fixture()
def prophane_test_dbs(tmpdir):
    source_db_dir = os.path.join(os.path.dirname(__file__), 'test_dbs')
    target_dir = tmpdir
    source_base_dir_name = os.path.basename(os.path.normpath(source_db_dir))
    copy(source_db_dir, str(target_dir))
    new_db_dir = str(target_dir.join(source_base_dir_name))
    return new_db_dir


@pytest.fixture()
def old_prophane_test_dbs(tmpdir):
    source_db_dir = os.path.join(os.path.dirname(__file__), 'test_dbs')
    db_dir = setup_test_dbs(source_db_dir, tmpdir)
    return db_dir


@pytest.fixture()
def prophane_test_dbs_before_map_gz(tmpdir):
    source_db_dir = os.path.join(os.path.dirname(__file__), 'test_dbs_before_map_gz')
    db_dir = setup_test_dbs(source_db_dir, tmpdir)
    return db_dir


@pytest.fixture()
def prophane_input():
    def _setup(test_dir, tmpdir, db_dir=''):
        """
        Copy files from test_dir/job_dir to tmpdir/job_dir
        and adjust preliminary paths of config file copied from test_dir/job_dir/
        :param test_dir: folder containing 'job_dir'
        :param tmpdir: 'test_dir/job_dir' is copied here
        :param db_dir: database base directory; needed for updating database paths in input config; if not specified,
            'tmpddir/test_dbs' is set as database base path
        :return:
            path of config file
            job directory
        """
        db_dir = db_dir if db_dir is not '' else os.path.join(tmpdir, 'test_dbs')
        assert os.path.isdir(db_dir)
        from tests import setup_test
        job_dir = os.path.join(tmpdir, 'job_dir')
        copy(os.path.join(os.path.dirname(__file__), test_dir, "job_dir"), tmpdir)
        yamls = setup_test.set_prophane_config_paths(
            db_base_dir=db_dir,
            prophane_base_dir=PROPHANE_DIR,
            job_base_dir=job_dir,
            general_config_path=os.path.join(job_dir, "general_config.yaml")
        )
        job_yamls = [yaml for yaml in yamls if 'input/' in os.path.relpath(yaml, job_dir)]
        assert len(job_yamls) == 1, 'got "{}" prophane job config yamls. Expected exactly 1.'\
            .format(len(yamls))
        return job_yamls[0], job_dir
    return _setup


@pytest.fixture()
def f_proteomic_search_result():
    def _setup(test_dir, tmpdir):
        """
        Copy files from test_dir/job_dir to tmpdir/job_dir
        :param test_dir: folder containing 'job_dir'
        :param tmpdir: 'test_dir/job_dir' is copied here
        :return:
            path of proteomic_search_result
            job directory
        """
        from tests.test_integration import copy
        job_dir = os.path.join(tmpdir, 'job_dir')
        test_data = os.path.join(os.path.dirname(__file__), test_dir, "job_dir")
        copy(test_data, tmpdir)
        lst_files = get_files_in_dir(job_dir)
        assert len(lst_files) == 1, "expected a single file in:\n\t'{job_dir}'\n\tfound: {n}" \
            .format(job_dir=job_dir, n=len(lst_files))
        proteomic_search_result = os.path.abspath(lst_files[0])
        return proteomic_search_result, job_dir
    return _setup