import os
import pandas as pd
import pytest
import utils.db_handling.db_access

from utils.db_handling.databases.helper_classes import Database
from utils.db_handling.db_access import DbAccessor


testdata = [
    ('tests/test_dbs/ncbi_nr/2018-08-08/nr.yaml', '2018-08-08', 'ncbi_nr', 'tax', True),
    ('tests/test_dbs/nr/2018-08-08/nr.yaml', '2018-10-08', 'ncbi_nr', 'tax', False),
    ('tests/test_dbs/pfams/31/Pfam-A.yaml', '31', 'pfams', 'func', True),
    ('tests/test_dbs/uniprot_sp/2018-08-08/uniprot_sprot.yaml', '2018-08-08', 'uniprot_sp', 'tax', True),
    ('tests/test_dbs/tigrfams/15.0/TIGRFAMs_15.0_HMM.yaml', '15.0', 'tigrfams', 'func', True),
    ('tests/test_dbs/uniprot_tr/2018-08-08/uniprot_trembl.yaml', '2018-08-08', 'uniprot_tr', 'tax', True),
    ('tests/test_dbs/uniprot_complete/2018-08-08/uniprot_complete.yaml', '2018-08-08', 'uniprot_complete', 'tax', True)
]
testdata_df = pd.DataFrame(testdata, columns=['db_yaml', 'db_version', 'db_type', 'scope', 'exists'])


@pytest.fixture()
def db_accessor(
        mocker,
        db_base_path="testing",
        test_df=testdata_df[['db_type', 'scope', 'db_version', 'db_yaml']].copy()
):
    test_df['db_yaml'] = test_df['db_yaml'].apply(lambda x: os.path.relpath(x, 'tests/test_dbs/'))
    mocker.patch.object(
        utils.db_handling.db_access.DbAccessor, '_read_database_info_file', return_value=test_df, autospec=True)
    mocker.patch.object(utils.db_handling.db_access, 'Databases', autospec=True)
    return utils.db_handling.db_access.DbAccessor(db_base_path)


@pytest.mark.parametrize("yaml,ver,db_type,scope,exists", testdata)
def test_return_of_db_version(yaml, ver, db_type, scope, exists):
    if exists:
        db_obj = Database(yaml)
        assert db_obj.get_version() == ver


@pytest.mark.parametrize("yaml,ver,db_type,scope,exists", testdata)
def test_return_of_db_type(yaml, ver, db_type, scope, exists):
    if exists:
        db = Database(yaml)
        assert db.get_type() == db_type


@pytest.mark.parametrize("yaml,ver,db_type,scope,exists", testdata)
def test_return_of_scope(yaml, ver, db_type, scope, exists):
    if exists:
        db = Database(yaml)
        assert db.get_scope() == scope


def test_fail_of_empty_database_yaml(mocker):
    mocker.patch.object(utils.db_handling.databases.helper_classes, 'load_yaml', return_value={})
    from utils.exceptions import NotAValidDatabaseConfigError
    with pytest.raises(NotAValidDatabaseConfigError):
        Database("path_does_not_matter")


# deactivated due to interconnectivity with Databases class. Dunno how to solve this atm
# @pytest.mark.parametrize("yaml,ver,db_type,scope,exists", testdata)
# def test_db_accessor(db_accessor, yaml, ver, db_type, scope, exists):
#     assert db_accessor.get_database_yaml(db_type, ver) == yaml


class TestDbAccessor:
    db_accessor_test_df = testdata_df[['db_type', 'scope', 'db_version', 'db_yaml']].copy()

    def test__read_database_info_file(self, mocker, db_base_path="testing", test_df=db_accessor_test_df.copy()):
        test_df['db_yaml'] = test_df['db_yaml'].apply(lambda x: os.path.relpath(os.path.abspath(x), db_base_path))
        mocker.patch.object(
            utils.db_handling.db_access.pd, 'read_csv', return_value=test_df.copy(), autospec=True)
        mocker.patch.object(utils.db_handling.db_access, 'Databases', autospec=True)
        DbAccessor(db_base_path)

    def test__read_database_info_file_TypeError_on_empty_file(self, mocker, db_base_path="testing"):
        mocker.patch.object(
            utils.db_handling.db_access.pd, 'read_csv', return_value=pd.DataFrame(), autospec=True)
        mocker.patch.object(utils.db_handling.db_access, 'Databases', autospec=True)
        with pytest.raises(TypeError) as pytest_wrapped_e:
            DbAccessor(db_base_path)
        assert str(pytest_wrapped_e.value) == \
               "Database overview file lacks required columns: '['db_type', 'db_version', 'db_yaml', 'scope']'\nfile: testing"

    def test__read_database_info_file_SystemExit_on_missing_file(self, mocker, db_base_path="testing"):
        mocker.patch.object(
            utils.db_handling.db_access.pd, 'read_csv', side_effect=FileNotFoundError, autospec=True)
        mocker.patch.object(utils.db_handling.db_access, 'Databases', autospec=True)
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            DbAccessor(db_base_path)
        assert pytest_wrapped_e.value.code == 1

    def test_print_summary(self, db_accessor):
        db_accessor.print_summary()

    def test_get_non_existant_db(self, db_accessor):
        from utils.exceptions import DatabaseAccessorKeyError
        with pytest.raises(DatabaseAccessorKeyError):
            db_accessor.get_database_yaml('wasd')


def test_database_yaml_writer_fails_for_existing_file(tmpdir):
    db_yaml = 'tests/test_dbs/ncbi_nr/2018-08-08/nr.yaml'
    db = Database(db_yaml)
    import os
    new_file = os.path.join(tmpdir, 'out.yaml')
    from pathlib import Path
    Path(new_file).touch()
    db.yaml = new_file
    with pytest.raises(FileExistsError):
        db.write_yaml()


def test_database_yaml_writer_force_for_existing_file(tmpdir):
    db_yaml = 'tests/test_dbs/ncbi_nr/2018-08-08/nr.yaml'
    db = Database(db_yaml)
    import os
    new_file = os.path.join(tmpdir, 'out.yaml')
    from pathlib import Path
    Path(new_file).touch()
    db.yaml = new_file
    db.write_yaml(force=True)
