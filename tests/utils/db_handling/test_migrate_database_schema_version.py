import pytest

import utils.db_handling.migrate_database_schema_version as mig_gz
from utils.db_handling.migrate_database_schema_version import assert_yaml_obj_has_target_version


def test_migrate_dbs_style_if_necessary(old_prophane_test_dbs, tmpdir):
    mig_gz.migrate_dbs_style_if_necessary(old_prophane_test_dbs)


def test_check_version_bump():
    from utils.db_handling.databases.helper_classes import Database
    db = Database("tests/test_dbs/ncbi_nr/2018-08-08/nr.yaml")
    version = db.get_db_schema_version()
    with pytest.raises(ValueError):
        assert_yaml_obj_has_target_version(db, version + 1)
