import os


def input_filter_idmapping_dat(wildcards):
    db_obj = get_db_object_for_dbtype_and_version("uniprot_tax", wildcards.db_version, wildcards.db_base_dir)
    idmap = db_obj.get_idmap()
    return idmap


rule filter_idmapping_dat:
    input:
        idmap=input_filter_idmapping_dat
    output:
        temp("{db_base_dir}/uniprot_tax/{db_version}/idmapping_filtered_for_ncbi_taxids.dat.gz")
    message:
        "filtering file for NCBI TaxID entries:\n\t{input.idmap}"
    shell:
        '''
        zcat {input.idmap} | grep -P '\tNCBI_TaxID\t' | cut -f1,3 | gzip > {output[0]}
        '''


def input_make_uniprot_taxmap(wildcards):
    db_obj = get_db_object_for_dbtype_and_version("uniprot_tax", wildcards.db_version, wildcards.db_base_dir)
    taxmap = db_obj.get_taxmap_txt() #download by this link https://www.uniprot.org/taxonomy/?sort=&desc=&compress=no&query=&fil=&force=no&preview=true&format=tab&columns=id

    idmap_filtered = os.path.join(
        wildcards.db_base_dir,
        "uniprot_tax",
        wildcards.db_version,
        "idmapping_filtered_for_ncbi_taxids.dat.gz")
    return [taxmap, idmap_filtered]


rule make_uniprot_acc2tax_map:
    input:
        input_make_uniprot_taxmap
    output:
        "{db_base_dir}/uniprot_tax/{db_version}/{taxmap}.map.gz",
        "{db_base_dir}/uniprot_tax/{db_version}/{taxmap}.unmapped.log.gz"
    message:
        "generating taxmap {output[0]}"
    benchmark:
        "{db_base_dir}/uniprot_tax/{db_version}/{taxmap}.map.gz.benchmark.txt"
    resources:
        mem_mb=1024*3
    version:
        "0.1"
    params:
        db_obj=lambda wildcards: get_db_object_for_dbtype_and_version("uniprot_tax", wildcards.db_version, wildcards.db_base_dir)
    run:
        params.db_obj.create_acc2tax_map(input[0], input[1], output[0], output[1])
