import gzip

def create_dbcan_map(tsv_in, map_out):
    with open(tsv_in, "r") as handle, gzip.open(map_out, "wt") as map_handle:
        map_handle.write("cazy family id\tdescription\n")
        for line in handle:
            fields = line.split("\t")
            if line.startswith("#") or len(fields) != 2:
                continue
            map_handle.write(fields[0].strip() + "\t" + fields[1].strip() + "\n")

rule make_dbcan_map:
    input:
        "{db_base_dir}/dbcan/{db_version}/{db}.fam-activities.txt"
    output:
        "{db_base_dir}/dbcan/{db_version}/{db}.map.gz",
    wildcard_constraints:
        fname = ".*/dbCAN-[^/]+"
    message:
        "generating map {output[0]}"
    benchmark:
        "{db_base_dir}/dbcan/{db_version}/{db}.map.gz.benchmark.txt"
    resources:
        mem_mb=400
    version:
        "0.1"
    run:
        create_dbcan_map(input[0], output[0])
