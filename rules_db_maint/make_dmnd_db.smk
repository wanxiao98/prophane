def input_make_dmnd_db(wildcards):
    db_obj = get_db_object_for_dbtype_and_version(wildcards.db_type, wildcards.db_version, config["db_base_dir"])
    db = db_obj.get_fasta_gz_file()
    acc2tax = db_obj.get_list_of_acc2tax_files()[0]
    taxdump = db_obj.get_taxdump_file()
    return [db, acc2tax, taxdump]

rule make_dmnd_db:
  input:
    input_make_dmnd_db
  output:
    dmnd_db="{db_base_dir}/{db_type}/{db_version}/{dmnddb}.dmnd",
    temp_dir=temp(directory("{db_base_dir}/{db_type}/{db_version}/{dmnddb}_TMP"))    # temporary dir is placed in DB folder as its size can exceed multiple GBs
  message:
    "preparing diamond db for first use: " + os.path.basename("{output[0]}")
  log:
    "{db_base_dir}/{db_type}/{db_version}/{dmnddb}.dmnd.log"
  version: "0.12"
  conda:
    "../envs/diamond.yaml"
  threads: 10
  benchmark:
    "{db_base_dir}/{db_type}/{db_version}/{dmnddb}.dmnd.benchmark.txt"
  resources:
    mem_mb=1024*5
  shell:
    '''
    mkdir {output.temp_dir}
    diamond makedb -t {output.temp_dir} --in {input[0]} --db {output.dmnd_db} --threads {threads} > {log} 2>&1
    '''
