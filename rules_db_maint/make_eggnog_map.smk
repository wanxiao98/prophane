import gzip
import io
import sys


def input_make_eggnog_map(wildcards):
    db_obj = get_db_object_for_dbtype_and_version("eggnog", wildcards.db_version, wildcards.db_base_dir)
    annotation_file = db_obj.get_og_annotations_file()
    funcat_file = db_obj.get_fun_cats_file()
    return [annotation_file, funcat_file]


rule make_eggnog_map:
    input:
        input_make_eggnog_map
    output:
        "{db_base_dir}/eggnog/{db_version}/{db}.map.gz"
    message:
        "generating eggnog database map ..."
    benchmark:
        "{db_base_dir}/eggnog/{db_version}/{db}.map.gz.benchmark.txt"
    resources:
        mem_mb=1500
    run:
        annotation_file = input[0]
        funcat_file = input[1]
        #read eggnog funcats
        funcats = {}
        with open(funcat_file, "r") as handle:
            for line in handle:
                line = line.strip()
                if len(line) == 0:
                    continue
                elif line[0] != "[":
                    mainrole = line.lower()
                else:
                    key = line[1]
                    subrole = line[4:]
                    if key in funcats:
                        sys.exit("error: multiple functional roles for key ''" + key + "' in " + funcat_file)
                    funcats[key] = (mainrole, subrole)

        #read eggnog og annotations
        na = set()
        with gzip.open(annotation_file, 'rb') as gzhandle, \
                gzip.open(output[0], 'wt') as outhandle:
            inhandle = io.BufferedReader(gzhandle)
            outhandle.write("#og.taxid.prot\tmain_role\tsub_role\tog\tdescr\tprot\n")
            for line in inhandle:
                line = line.decode().strip()
                if len(line) == 0:
                    continue
                fields = line.split("\t")
                og = fields[0]
                prots = fields[-1].split(",")
                descr = fields[3]
                fun_key = fields[4].split("'")[1]
                mainrole = funcats[fun_key][0]
                subrole = funcats[fun_key][1]
                for prot in prots:
                    if prot not in na:
                        na.add(prot)
                        outhandle.write(
                            "NA" + "." + prot + "\t" + mainrole + "\t" + subrole + "\t" + og + "\t" + descr + "\t" + prot + "\n")
                    outhandle.write(
                        og + "." + prot + "\t" + mainrole + "\t" + subrole + "\t" + og + "\t" + descr + "\t" + prot + "\n")
