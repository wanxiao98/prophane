import os

rule download_database_file:
    output:
        "{db_base_dir}/{database}/{version}/downloaded/{filename}"
    message:
        f"downloading file: {os.path.basename('{output[0]}')}"
    version: "0.1"
    params:
        # file=db.get_link(),
        url="{filename}",
        md5sum=""
    shell:
        """
        DL_DIR="{wildcards.db_base_dir}/{wildcards.database}/{wildcards.version}/downloaded/"
        cp -t $DL_DIR {params.url}
        # wget --directory-prefix=$DL_DIR "{params.url}"
        cd $DL_DIR
        if [ -f "{params.md5sum}" ]; then
            md5sum -c "{params.md5sum}"
        elif [ "{params.md5sum}" = "" ]; then
            true
        else
            echo "{params.md5sum} {wildcards.filename}" | md5sum -c -
        fi
        """
