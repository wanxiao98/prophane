from utils.config import INCONCLUSIVE_ANNOTATIONS
import pandas as pd

def create_pfam_map(clan_in, pfam_in, pfam_out):
    df_clans = pd.read_csv(clan_in, sep='\t', usecols=[0, 1, 3], names=['clan', 'symbol', 'cdescr'])
    df_pfams = pd.read_csv(pfam_in, sep='\t', usecols=[0, 1, 4], names=['pfam', 'clan', 'pdescr'])

    # merge clan and pfam description on shared clan accession
    df_merged = df_pfams.merge(df_clans, on='clan', how='outer')
    # replace nan from missing clan accessions w/ default 'unclassified' values
    clan_unclassified = INCONCLUSIVE_ANNOTATIONS['unclassified']
    nan_values = {'clan': clan_unclassified, 'symbol': '-', 'cdescr': '-'}
    df_merged = df_merged.fillna(value=nan_values)
    # deleting trailing whiespaces in descr
    df_merged['pdescr'] = df_merged['pdescr'].str.strip()
    df_merged['cdescr'] = df_merged['cdescr'].str.strip()
    # sort by pfam accession and reorder columns
    df_merged = df_merged.sort_values(by='pfam')
    df_merged = df_merged[['pfam', 'clan', 'symbol', 'cdescr', 'pdescr', 'pfam']]
    # write out pfam.map.gz
    pfam_header = ['pfam', 'clan', 'clan_symbol', 'clan_descr', 'pfam_descr', 'pfam']
    df_merged.to_csv(pfam_out, sep='\t', header=pfam_header, index=False, compression='gzip')


rule make_pfam_map:
    input:
        clan_in = "{pfamsdb}.clan.txt.gz", #source: ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/clan.txt.gz
        pfam_in = "{pfamsdb}.clans.tsv.gz"
    output:
        pfam_out = "{pfamsdb}.map.gz"
    message:
        "generating map {output[0]}"
    benchmark:
        "{pfamsdb}.map.gz.benchmark.txt"
    resources:
        mem_mb=400
    wildcard_constraints:
        pfamsdb = ".*/Pfam[^/]+"
    run:
        create_pfam_map(input.clan_in, input.pfam_in, output.pfam_out)

